const functions = require('firebase-functions');
const admin = require('firebase-admin')
admin.initializeApp();

exports.getUserTest = functions.https.onRequest((user, response) => {

    let ref = admin.database().ref('users');

    ref.on("value", function (snapshot) {
        response.send(snapshot.val());

    })
});

exports.getUserOnCall = functions.region('europe-west1').https.onCall((data, context) => {
    let ref = admin.database().ref('users');

    return ref.once('value').then(snapshot => {
        const skoczekDataString = JSON.stringify(snapshot.val());
        const skoczekDataObjects = JSON.parse(skoczekDataString);

        let arr = [];
        for (let item in skoczekDataObjects) {
            let objectJson = "{" + JSON.stringify(item) + ":" + JSON.stringify(skoczekDataObjects[item]) + "}";
            let object = JSON.parse(objectJson);
            arr.push(object);
        }

        //sort arr ascending
        arr.sort((a, b) => {
            for (first_a in a) break;
            for (first_b in b) break;
            return b[first_b].ranking - a[first_a].ranking
        })

        //get ranking positions
        let rankPosWorld = 0
        let rankPosCountry = 0
        let rankPosCity = 0


        for (let element of arr) {
            userName = element;
            for (child in element) break
            if (element[child].country === data.country) {
                rankPosCountry++
                if (element[child].city === data.city) {
                    rankPosCity++
                }
            }
            if (element.hasOwnProperty(data.name)) {
                rankPosWorld = arr.indexOf(element) + 1
                break
            }
        }

        let functionResp = {
            "rankWorld": rankPosWorld,
            "rankCountry": rankPosCountry,
            "rankCity": rankPosCity
        }

        return JSON.stringify(functionResp);
    })
        .catch((error) => {
            throw new functions.https.HttpsError('unknown', error.message, error);
        });
});

exports.getUsersFromCountry = functions.region('europe-west1').https.onCall((data, context) => {
    let refDb = admin.database().ref('users');
    return refDb.once('value').then(snapshot => {

        const skoczekDataString = JSON.stringify(snapshot.val());
        const skoczekDataObjects = JSON.parse(skoczekDataString);
        let arr = [];

        for (let item in skoczekDataObjects) {
            let nam = "{" + JSON.stringify("name") + ":" + JSON.stringify(item)+"}"
            let myJson = JSON.parse(nam)
            myJson["city"] = skoczekDataObjects[item]['city']
            myJson["country"] = skoczekDataObjects[item]['country']
            myJson["ranking"] = skoczekDataObjects[item]['ranking']
            arr.push(myJson);
        }

        arr.sort((a, b) => {
            return b['ranking'] - a['ranking']
        })
        let arrCountry = []
        let arrCity = []
        if(data.returnKey==='country'){
            arrCountry = arr.filter(function (el) {
                return el.country === data.country
            })
            const resultArrCountry = arrCountry.slice(0,50);
            return "{"+"\"response\":"+JSON.stringify(resultArrCountry)+"}"
        } else if (data.returnKey==='city'){
            arrCountry = arr.filter(function (el) {
                return el.country === data.country
            })
            arrCity = arrCountry.filter(function (elem) {
                return elem.city === data.city
            })
            const resultArrCity = arrCity.slice(0,50);
            return "{"+"\"response\":"+JSON.stringify(resultArrCity)+"}"
        } else {
            return {"error":"data error"}
        }
    })
        .catch((error) => {
            throw new functions.https.HttpsError('unknown', error.message, error);
        });
});